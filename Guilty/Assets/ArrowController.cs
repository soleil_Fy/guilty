﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowController : MonoBehaviour
{

    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("player");
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(0.01f,0, 0);   //一定の速さで落下する

        if (transform.position.x < -5.0f) //画面外にオブジェクトが出たらなくなる
        {
            Destroy(gameObject);
        }

        //当たり判定
        Vector2 p1 = transform.position;    //矢の中心座標
        Vector2 p2 = player.transform.position;     //プレイヤーの中心座標
        Vector2 dir = p1 - p2;
        float d = dir.magnitude; //dirの大きさ
        float r1 = 0.5f;    //矢の半径
        float r2 = 1.0f;    //プレイヤーの半径

        if (d < r1 + r2)
        {
            Destroy(gameObject);    //当たったら矢が消える
            GameObject gameDirector = GameObject.Find("GameDirector");
            gameDirector.GetComponent<GemeDirector>().DecreaseHp();        //矢がプレイヤーに衝突したことをしらせる
        }
    }
}
