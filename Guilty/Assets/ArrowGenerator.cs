﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowGenerator : MonoBehaviour
{

    public GameObject arrowPrefab;
    public float span = 1.0f;   //矢の落ちる速さ
    float delta = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;    //ゲームの時間
        Debug.Log(delta);      //ランダムな時間
        if (delta > span)      //〇秒に一個矢が落ちる
        {
            delta = 0;


            GameObject go = Instantiate(arrowPrefab);
            float px = Random.Range(-6.0f, 7.0f);   //－６～６までのランダムな数字
            go.transform.position = new Vector3(px, 7, 0);
        }
    }
}
